package com.stratio.challenge.presencecontrol.services;

import java.util.List;

import com.stratio.challenge.presencecontrol.model.EmploymentRecord;

public interface EmploymentRecordService {


	public EmploymentRecord clockIn(Integer idUsuario);
	
	public List<EmploymentRecord> findByidUsuario(Integer idUsuario);
	
	public EmploymentRecord clockOut(Integer idUsuario);

	public EmploymentRecord manualRegistry(EmploymentRecord employmentRecord);

	public void manualRegistryAbsence(EmploymentRecord employmentRecord);
}
