package com.stratio.challenge.presencecontrol.repositories;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.stratio.challenge.presencecontrol.model.EmploymentRecord;

public interface EmploymentRecordRepository extends MongoRepository<EmploymentRecord, String> {


	public List<EmploymentRecord> findByIdUsuario(Integer idUsuario);
	
	public EmploymentRecord findByIdUsuarioAndFinishWorkIsNull(Integer idUsuario);
	
	public List<EmploymentRecord> findByIdUsuarioAndStartWorkBetweenOrFinishWorkBetween(Integer idUsuario,LocalDateTime start,LocalDateTime finish,LocalDateTime startEnd,LocalDateTime finishEnd);

	public List<EmploymentRecord> findByIdUsuarioAndStartWorkBeforeAndFinishWorkAfter(Integer idUsuario,LocalDateTime start,LocalDateTime finish);
}