package com.stratio.challenge.presencecontrol.model;

import java.time.Duration;
import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class EmploymentRecord {

	@Id
	private String id;

	private Integer idUsuario;
	private LocalDateTime startWork;
	private LocalDateTime finishWork;

	@Transient
	private Long minutesWork;

	@Transient
	private Long hoursWork;

	public EmploymentRecord() {
		super();
	}

	public EmploymentRecord(Integer idUsuario) {
		super();
		this.idUsuario = idUsuario;
	}

	public EmploymentRecord(Integer idUsuario, LocalDateTime startWork) {
		super();
		this.idUsuario = idUsuario;
		this.startWork = startWork;
	}
	
	public EmploymentRecord(String id, Integer idUsuario, LocalDateTime startWork, LocalDateTime finishWork) {
		super();
		this.id = id;
		this.idUsuario = idUsuario;
		this.startWork = startWork;
		this.finishWork = finishWork;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public LocalDateTime getStartWork() {
		return startWork;
	}

	public void setStartWork(LocalDateTime startWork) {
		this.startWork = startWork;
	}

	public LocalDateTime getFinishWork() {
		return finishWork;
	}

	public void setFinishWork(LocalDateTime finishWork) {
		this.finishWork = finishWork;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Long getMinutesWork() {
		if (startWork != null && finishWork != null) {
			return Duration.between(startWork, finishWork).toMinutes() % 60;
		} else {
			return 0L;
		}
	}

	public Long getHoursWork() {
		if (startWork != null && finishWork != null) {
			return Duration.between(startWork, finishWork).toHours();
		} else {
			return 0L;
		}
	}

}
