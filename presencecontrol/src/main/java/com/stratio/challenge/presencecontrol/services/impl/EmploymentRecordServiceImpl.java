package com.stratio.challenge.presencecontrol.services.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stratio.challenge.presencecontrol.model.EmploymentRecord;
import com.stratio.challenge.presencecontrol.repositories.EmploymentRecordRepository;
import com.stratio.challenge.presencecontrol.services.EmploymentRecordService;

@Service
public class EmploymentRecordServiceImpl implements EmploymentRecordService {

	@Autowired
	EmploymentRecordRepository employmentRecordRepository;

	public EmploymentRecord clockIn(Integer idUsuario) {
		return ((EmploymentRecord) employmentRecordRepository
				.save(new EmploymentRecord(idUsuario, LocalDateTime.now())));
	}

	public List<EmploymentRecord> findByidUsuario(Integer idUsuario) {
		return ((List<EmploymentRecord>) employmentRecordRepository.findByIdUsuario(idUsuario));
	}

	public EmploymentRecord clockOut(Integer idUsuario) {
		EmploymentRecord employmentRecord = employmentRecordRepository.findByIdUsuarioAndFinishWorkIsNull(idUsuario);
		employmentRecord.setFinishWork(LocalDateTime.now());
		return (EmploymentRecord) employmentRecordRepository.save(employmentRecord);
	}

	@Override
	public EmploymentRecord manualRegistry(EmploymentRecord employmentRecord) {
		List<EmploymentRecord> recordInDate = employmentRecordRepository.findByIdUsuarioAndStartWorkBetweenOrFinishWorkBetween(
				employmentRecord.getIdUsuario(), employmentRecord.getStartWork(), employmentRecord.getFinishWork(),
				employmentRecord.getStartWork(), employmentRecord.getFinishWork());
		// Should only be one or nothing in this period
		if (!recordInDate.isEmpty()) {
			// if there is a work period. We Update the start or finish making a
			// bigger period
			EmploymentRecord recordToUpdateFinishWork = recordInDate.get(0);

			if (employmentRecord.getStartWork().isBefore(recordToUpdateFinishWork.getStartWork())) {
				recordToUpdateFinishWork.setStartWork(employmentRecord.getStartWork());
			}

			if (employmentRecord.getFinishWork().isAfter(recordToUpdateFinishWork.getFinishWork())) {
				recordToUpdateFinishWork.setFinishWork(employmentRecord.getFinishWork());
			}
			// Updated the record
			return employmentRecordRepository.save(recordToUpdateFinishWork);
		} else {
			recordInDate = employmentRecordRepository.findByIdUsuarioAndStartWorkBeforeAndFinishWorkAfter(employmentRecord.getIdUsuario(),employmentRecord.getStartWork(),employmentRecord.getFinishWork());
			if (!recordInDate.isEmpty()) {
				//Nothing changes. No modify period
				return recordInDate.get(0);
			}
			else{
				//Create a new one
				return employmentRecordRepository.save(employmentRecord);
			}
		}
	}

	@Override
	public void manualRegistryAbsence(EmploymentRecord employmentRecord) {
		List<EmploymentRecord> recordInDate = employmentRecordRepository.findByIdUsuarioAndStartWorkBetweenOrFinishWorkBetween(
				employmentRecord.getIdUsuario(), employmentRecord.getStartWork(), employmentRecord.getFinishWork(),
				employmentRecord.getStartWork(), employmentRecord.getFinishWork());
		// Should only be one or nothing in this period
		if (!recordInDate.isEmpty()) {
			for (EmploymentRecord record : recordInDate) {
				employmentRecordRepository.delete(record);
			}
		}
	}

}
