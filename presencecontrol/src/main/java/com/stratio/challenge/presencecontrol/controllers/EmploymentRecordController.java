package com.stratio.challenge.presencecontrol.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.stratio.challenge.presencecontrol.model.EmploymentRecord;
import com.stratio.challenge.presencecontrol.services.EmploymentRecordService;

@RestController 
@RequestMapping("/employmentrecord")
public class EmploymentRecordController {

	
	/** The logger. */
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
    /** The employment record service. */
    @Autowired
    EmploymentRecordService employmentRecordService;

    
    /**
     * Manual registry.Insert manually new employment record 
     *
     * @param employmentRecord the employment record
     * @return the employment record
     */
    @PostMapping()
    @ResponseStatus(value = HttpStatus.OK)
    public EmploymentRecord manualRegistry(@RequestBody EmploymentRecord employmentRecord) {
        return employmentRecordService.manualRegistry(employmentRecord);
    }
    
    /**
     * Manual registry abscence .Insert an abscence deleting all periods between dates
     *
     * @param employmentRecord the employment record
     * @return the employment record
     */ 
    @DeleteMapping()
    @ResponseStatus(value = HttpStatus.OK)
    public void manualRegistryAbsence(@RequestBody EmploymentRecord employmentRecord) {
        employmentRecordService.manualRegistryAbsence(employmentRecord);
    }
    
    /**
     * Register Clock in of one user.
     *
     * @param idUsuario the id usuario
     * @return the employment record
     */
    @PostMapping(value="/idusuario/{idUsuario}")
    @ResponseStatus(value = HttpStatus.OK)
    public EmploymentRecord clockIn(@PathVariable Integer  idUsuario) {
        return employmentRecordService.clockIn(idUsuario);
    }
    
    /**
     * Register Clock out of one user .
     *
     * @param idUsuario the id usuario
     * @return the employment record
     */
    @PutMapping(value="/idusuario/{idUsuario}")
    @ResponseStatus(value = HttpStatus.OK)
    public EmploymentRecord clockOut(@PathVariable Integer  idUsuario) {
        return (EmploymentRecord) employmentRecordService.clockOut(idUsuario);
    }
    
    /**
     * Find by id usuario.
     *
     * @param idUsuario the id usuario
     * @return the list
     */
    @GetMapping(value="/idusuario/{idUsuario}")
    @ResponseStatus(value = HttpStatus.OK)
    public List<EmploymentRecord> findByIdUsuario(@PathVariable Integer  idUsuario) {
        return employmentRecordService.findByidUsuario(idUsuario);
    }
    
}
