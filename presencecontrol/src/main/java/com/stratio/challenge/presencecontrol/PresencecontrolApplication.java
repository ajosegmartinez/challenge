package com.stratio.challenge.presencecontrol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = { "com.stratio.challenge.presencecontrol.*" })
public class PresencecontrolApplication {

	public static void main(String[] args) {
		SpringApplication.run(PresencecontrolApplication.class, args);
	}
}
